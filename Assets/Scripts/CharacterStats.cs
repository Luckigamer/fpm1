﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public float characterHealt = 100f;
    public float characterSpeed = 3.5f;
    public bool isMoving;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (characterHealt <= 0f)
        {
            Destroy(this.gameObject);
        }
    }

    public void CharacterHit()
    {
        characterHealt -= 25f;
    }
}
