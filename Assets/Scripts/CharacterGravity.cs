﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGravity : MonoBehaviour
{
    public CharacterController enemyRobot;

    private Vector3 velocity;
    private float gravity = -9.81f;
    public float groundDistance = 0.4f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Gravity
        velocity.y += gravity * Time.deltaTime;
        enemyRobot.Move(velocity * Time.deltaTime);
    }
}
