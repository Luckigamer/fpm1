﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Grenade : MonoBehaviour
{
    public bool isEnabled;
    public float timeToExplode = 3;
    public GameObject particle;

    private bool isThrown = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isThrown && isEnabled)
        {
            if (timeToExplode > 0)
            {
                timeToExplode -= Time.deltaTime;
            }
            else
            {
                PhotonView pv = GetComponent<PhotonView>(); 
                pv.RPC("ExplodeGrenade", RpcTarget.All);
            }
        }
    }

    [PunRPC]
    public void ExplodeGrenade()
    {
        GameObject particleObject = Instantiate(particle, transform.position, transform.rotation);
        ParticleSystem ps = particleObject.GetComponent<ParticleSystem>();
        float timeToDestroy = ps.main.duration;
        Destroy(gameObject);
        Destroy(particleObject, timeToDestroy);
    }

    public void onThrow()
    {
        isThrown = true;
    }
}
