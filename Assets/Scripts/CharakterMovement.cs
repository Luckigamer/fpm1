﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class CharakterMovement : MonoBehaviour
{

    public CharacterController player;
    public Animator animator;
    public CharacterStats playerStats;

    public float mouseSensivity = 200f;
    private float movementSpeed = 3.5f;
    private Vector3 velocity;
    private float gravity = -9.81f;
    public float groundDistance = 0.4f;
    public Transform cameraTransform;
    public Transform playerTransform;
    public LayerMask groundMask;
    private bool isGrounded;
    private bool isHoldingPistol = false;
    private float jumpHight = 1.5f;
    public GameObject grenadePrefab;
    private GameObject gun;
    public Volume postProcessingVolume;

    float xRotaion = 0f;

    bool lastMovementState;

    private PhotonView pv;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        pv = GetComponent<PhotonView>();
        if(!pv.IsMine)
        {
            Camera cam =  cameraTransform.GetComponent <Camera>();
            cam.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (pv.IsMine)
        {
            //Gravity
            velocity.y += gravity * Time.deltaTime;
            player.Move(velocity * Time.deltaTime);

            //cameraTransform
            isGrounded = Physics.CheckSphere(playerTransform.position, groundDistance, groundMask);
            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            UpdateIsMoving();
            Move();
            Animate();
        }
}

    private void Move()
    {
        //Move
        if (Input.GetKey(KeyCode.W))
        {
            player.Move(player.transform.forward * Time.deltaTime * movementSpeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            player.Move(player.transform.forward * Time.deltaTime * -1 * movementSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            player.Move(player.transform.right * Time.deltaTime * -1 * movementSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            player.Move(player.transform.right * Time.deltaTime * movementSpeed);
        }
        //Look
        float mouseX = Input.GetAxis("Mouse X") * mouseSensivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensivity * Time.deltaTime;

        xRotaion -= mouseY;
        xRotaion = Mathf.Clamp(xRotaion, -90f, 90f);

        cameraTransform.localRotation = Quaternion.Euler(xRotaion, 0f, 0f);
        player.transform.Rotate(Vector3.up * mouseX);
        //Jump
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            velocity.y = Mathf.Sqrt(jumpHight * -2f * gravity);
        }
        //Run
        if (Input.GetKey(KeyCode.LeftShift))
        {
            movementSpeed = 5.55f;
        }
        //Run stop
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            movementSpeed = 3.5f;
        }
        //Walk
        if (Input.GetKey(KeyCode.LeftControl))
        {
            movementSpeed = 1.3f;
        }
        //Walk stop
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            movementSpeed = 3.5f;

        }
        //Interact
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.Raycast(new Ray(cameraTransform.position, cameraTransform.forward), out hit, 3f))
            {
                Debug.DrawRay(cameraTransform.position, cameraTransform.forward * 3f, Color.yellow, 10f);
                Debug.Log(hit.transform.name);
                if (hit.collider.gameObject.layer == 9)
                {                 
                    HoldObject(hit);
                }
            }
        }

        // Shooting
        if (isHoldingPistol && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Gun gunScript = gun.GetComponent<Gun>();
            gunScript.shoot();
            if (Physics.Raycast(new Ray(cameraTransform.position, cameraTransform.forward), out hit, 100f))
            {
                Debug.DrawRay(cameraTransform.position, cameraTransform.forward * 100f, Color.blue, 10f);
                Debug.Log(hit.transform.name);
                if (hit.collider.gameObject.layer == 10)
                {
                    Vignette vig;
                    postProcessingVolume.profile.TryGet<Vignette>(out vig);
                    vig.intensity.Override(1f);
                    CharacterStats enemyStats = (CharacterStats)hit.transform.GetComponent("CharacterStats");
                    enemyStats.CharacterHit();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            GameObject grenade = PhotonNetwork.Instantiate("Grenade", cameraTransform.position, Quaternion.identity);
            Grenade g = (Grenade) grenade.GetComponent("Grenade");
            g.onThrow();
            Rigidbody r = grenade.GetComponent<Rigidbody>();
            r.AddForce(cameraTransform.forward * 5f, ForceMode.Impulse);
        }
    }
    //Hold Object
    private void HoldObject(RaycastHit hit)
    {
        if (hit.transform.name == "Gun")
        {
            pv.RPC("spawnGunInHands", RpcTarget.All);
        }
    }


    [PunRPC]
    public void spawnGunInHands()
    {
        Transform characterTransform = pv.GetComponent<Transform>();
        isHoldingPistol = true;
        gun = PhotonNetwork.Instantiate("Gun", Vector3.zero, Quaternion.identity);
        Gun gunScript = gun.GetComponent<Gun>();
        gun.transform.parent = characterTransform;
        gunScript.pickUp();
    }

    private void UpdateIsMoving()
    {
     playerStats.isMoving = (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)); 
    }

    private void Animate()
    {
        animator.SetBool("isWalking", playerStats.isMoving);
        animator.SetBool("hasPistol", isHoldingPistol);
    }
}
