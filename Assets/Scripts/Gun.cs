﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class Gun : MonoBehaviour
{
    public GameObject particle;
    public Transform muzzlePosition;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    public void shoot()
    {
        PhotonView pv = GetComponent<PhotonView>();
        pv.RPC("showParticle", RpcTarget.All);

    }

    public void pickUp()
    {
        PhotonView pv = GetComponent<PhotonView>();
        pv.RPC("moveToHands", RpcTarget.All);
    }

    [PunRPC]
    public void showParticle()
    {
        GameObject particleObject = Instantiate(particle, muzzlePosition.position, muzzlePosition.rotation);
        ParticleSystem LaserBeam = particleObject.GetComponent<ParticleSystem>();
        //LaserBeam.Play();
    }

    [PunRPC]
    public void moveToHands()
    {
        transform.localPosition = new Vector3(0.14f, 1.812f, 0.559f);
        transform.eulerAngles = new Vector3(0f, 289.619f, 0f);
    }
}

