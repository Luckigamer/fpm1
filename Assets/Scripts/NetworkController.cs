﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class NetworkController : MonoBehaviourPunCallbacks
{

    int RoomSize = 4;

    // Start is called before the first frame update
    void Start()
    {
        //Connect to Photon Master Server
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.CloudRegion);
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.JoinRoom("Test");
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;
        roomOptions.MaxPlayers = (byte)RoomSize;
        roomOptions.BroadcastPropsChangeToAll = true;
        PhotonNetwork.CreateRoom("Test", roomOptions);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.CurrentRoom.Name);
        CreatePlayer();
    }

    private void CreatePlayer()
    {
        Debug.Log("Create Player");
        PhotonNetwork.Instantiate("Player", new Vector3(12.031f, 3.839f, -6.97f), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}